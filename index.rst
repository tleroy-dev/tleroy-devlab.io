.. theo documentation documentation master file, created by
   sphinx-quickstart on Wed Apr  8 15:31:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



.. toctree::
   :maxdepth: 3
   
   shell
   mcuxpresso
   helper
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
